# [OWASP](https://www.owasp.org) Threat Dragon #

An online threat modelling web application including system diagramming and a rule engine to auto-generate threats/mitigations. 
The focus will be on great UX, a powerful rule engine and integration with other development lifecycle tools.

We are currently maintaining [a working protoype](http://babydragon.azurewebsites.net/#/) in sych with the main code branch.

An [OWASP Incubator Project](https://www.owasp.org/index.php/OWASP_Threat_Dragon).

**Project leader:** Mike Goodwin (mike.goodwin@owasp.org)

##Getting started

ThreatDragon is a Single Page Application (SPA) using Angular on the client and node.js on the server, althought the server side code does almost nothing so far.
To build and run locally, follow these steps:

Install Git and node.js. If you want to run the unit tests, install recent vesions of Chrome, Firefox and IE. Threat Dragon uses Bower and Grunt for its build workflow, so

`npm install -g bower grunt grunt-cli`

To get the code, go to where you want your code to be located and do

`git init`

`git clone https://bitbucket.org/theblacklabrador/threatmodellingtool.git`

This installs code in two sub-folders. One for the main application (`td`) and one for the unit tests (`threatdragon.tests`). In the `td` folder get all the node
packages:

`npm install`

All the build workflow task are in the default grunt task, so just do

`grunt`

and then start the node web server

`node bin\www`

If you then browse to `http://localhost:3000` you should see the running application.


More documentation will follow soon...